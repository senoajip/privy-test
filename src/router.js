import Vue from 'vue'
import Router from 'vue-router'
import Login from './components/Login.vue'
import Register from './components/Register.vue'
import Profil from './components/Profil.vue'
import Otp from './components/Otp.vue'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {path: '/', component: Login},
        {path: '/register', component: Register},
        {path: '/profil', component: Profil},
        {path: '/otp', component: Otp},
    ]
})